﻿using UnityEngine;

public class BoatController : MonoBehaviour
{
    [SerializeField] private AnimationCurve _speedCurve;
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField] [Range(0, 100)] private float _tangleProbabilty;
    [SerializeField] private float _tangleMin, _tangleMax;
    
    private void OnValidate()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }
    
    private void FixedUpdate()
    {
        _rigidbody2D.position += Vector2.right * _speedCurve.Evaluate(Time.timeSinceLevelLoad) * Time.deltaTime;
        if (Random.Range(0, 101) <= _tangleProbabilty)
        {
            _rigidbody2D.AddForce(Vector2.down * Random.Range(_tangleMin, _tangleMax), ForceMode2D.Impulse);
        }
    }
}