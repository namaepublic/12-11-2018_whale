﻿using UnityEngine;

public class SkyController : MonoBehaviour
{
	[SerializeField] private Transform[] _cloudPresets;
	[SerializeField] private float _presetLength;
	[SerializeField] private Transform _cameraTransform;

	private float _previousX;
	private int _presetIndex;

	private void Start()
	{
		_previousX = 0;
		_presetIndex = 0;
	}

	private void Update()
	{
		if (_cameraTransform.position.x - _previousX >= _presetLength)
		{
			_cloudPresets[_presetIndex].position += Vector3.right * _cloudPresets.Length * _presetLength;
			_previousX = _cameraTransform.position.x;
			_presetIndex++;
			if (_presetIndex >= _cloudPresets.Length)
				_presetIndex = 0;
		}
	}
}
