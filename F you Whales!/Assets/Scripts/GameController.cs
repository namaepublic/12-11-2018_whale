﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
	[SerializeField] private int _lifeCount;
	[SerializeField] private Image[] _lifeImages;
	[SerializeField] private TextMeshProUGUI _scoreText;

	private int _currentLife;
	private int _score;

	private void Start()
	{
		_currentLife = _lifeCount;
		_score = 0;
		_scoreText.text = _score.ToString();
		foreach (var lifeImage in _lifeImages)
			lifeImage.color = Color.white;
	}

	public void LoseLife()
	{
		_currentLife--;
		_lifeImages[_currentLife].color = Color.black;
		if (_currentLife <= 0)
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		}
	}

	public void IncreaseScore()
	{
		_score++;
		_scoreText.text = _score.ToString();
	}
}
