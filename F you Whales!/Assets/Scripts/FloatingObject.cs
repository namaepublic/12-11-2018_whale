﻿using UnityEngine;

public class FloatingObject : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField] private float _waterLevel = -1;
    [SerializeField] private float _waterDensity = 0.25f;
    [SerializeField] private float _floatingTreshold = 1;
    [SerializeField] private float _downForce;

    private float _forceFactor;
    private Vector2 _floatingForce;
    
    private void OnValidate()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        _forceFactor = 1 - (_rigidbody2D.position.y - _waterLevel) / _floatingTreshold;
        if (_forceFactor > 0)
        {
            _floatingForce = -Physics2D.gravity * (_forceFactor - _rigidbody2D.velocity.y * _waterDensity);
            _floatingForce += Vector2.down * _downForce;
            _rigidbody2D.AddForceAtPosition(_floatingForce, _rigidbody2D.position);
        }
    }
}