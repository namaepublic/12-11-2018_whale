﻿using UnityEngine;

public class ArrowController : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField] private Transform _gunTransform;

    private bool _shooted;

    private void OnValidate()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    public ArrowController Init(Transform gunTransform)
    {
        _gunTransform = gunTransform;

        return this;
    }

    private void Update()
    {
        if (_shooted || _gunTransform == null) return;
        transform.rotation = _gunTransform.rotation;
        transform.position = _gunTransform.position;
    }

    public void Shoot(float speed)
    {
        _shooted = true;
        _rigidbody2D.isKinematic = false;
        _rigidbody2D.AddForce(transform.right * speed, ForceMode2D.Impulse);
        Destroy(gameObject, 5);
    }
}