﻿using UnityEngine;

public class WhaleController : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField] [Range(0, 100)] private float _tangleProbabilty;
    [SerializeField] private float _tangleMin, _tangleMax;

    [SerializeField] private int _requiredHit;

    private int _takenHits;
    private GameController _gameController;

    private void OnValidate()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        _takenHits = 0;
        _gameController = FindObjectOfType<GameController>();
    }

    private void FixedUpdate()
    {
        if (Random.Range(0, 101) <= _tangleProbabilty)
        {
            _rigidbody2D.AddForce(Vector2.down * Random.Range(_tangleMin, _tangleMax), ForceMode2D.Impulse);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Arrow"))
        {
            _takenHits++;
            if (_takenHits >= _requiredHit)
            {
                _gameController.IncreaseScore();
                Destroy(other.gameObject);
                Destroy(gameObject);
            }
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("WhaleGoal"))
        {
            _gameController.LoseLife();
            Destroy(gameObject);
        }
    }
}