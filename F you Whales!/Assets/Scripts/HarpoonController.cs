﻿using UnityEngine;

public class HarpoonController : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] private Transform _targetTransform;
    [SerializeField] private Transform _gunTransform;
    [SerializeField] private GameObject _arrowPrefab;
    [SerializeField] private float _speed;
    [SerializeField] private float _cooldown;

    private bool _shoot, _shooted;
    private float _currentTime;
    private ArrowController _arrowController;

    private void OnValidate()
    {
        _camera = FindObjectOfType<Camera>();
    }

    private void Start()
    {
        _shoot = false;
        _shooted = false;
        _currentTime = 0;
        SpawnArrow();
    }

    private void Update()
    {
        _targetTransform.position = _camera.ScreenToWorldPoint(Input.mousePosition + Vector3.forward * 10);
        var dir = _targetTransform.position - _gunTransform.position;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        _gunTransform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        if (Input.GetMouseButtonDown(0))
            _shoot = true;

        if (_shooted)
        {
            if (_currentTime >= _cooldown)
                SpawnArrow();
            else
                _currentTime += Time.deltaTime;
        }
    }


    private void FixedUpdate()
    {
        if (_shoot && _arrowController != null)
        {
            _arrowController.Shoot(_speed);
            _shoot = false;
            _shooted = true;
        }
    }

    private void SpawnArrow()
    {
        _currentTime = 0;
        _arrowController = Instantiate(_arrowPrefab, _gunTransform.position, _gunTransform.rotation).GetComponent<ArrowController>()
            .Init(_gunTransform);
        _shooted = false;
    }
}