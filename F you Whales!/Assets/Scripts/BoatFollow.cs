﻿using UnityEngine;

public class BoatFollow : MonoBehaviour
{
    [SerializeField] private Transform _boatTransform;
    [SerializeField] private Vector3 _offset;

    private void FixedUpdate()
    {
        var destination = _boatTransform.position + _offset;
        destination.y = _offset.y;
        transform.position = destination;
    }
}
