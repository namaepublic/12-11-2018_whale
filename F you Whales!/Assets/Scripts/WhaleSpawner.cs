﻿using UnityEngine;

public class WhaleSpawner : MonoBehaviour
{
    [SerializeField] private GameObject _whalePrefab;
    [SerializeField] private Transform _boatTransform;
    [SerializeField] private AnimationCurve _cooldownCurve;
    [SerializeField] private Vector2 _minPosition, _maxPosition;

    private float _currentTime;

    private void Start()
    {
        _currentTime = _cooldownCurve.Evaluate(0);
    }

    private void Update()
    {
        if (_currentTime >= _cooldownCurve.Evaluate(Time.timeSinceLevelLoad))
        {
            Instantiate(_whalePrefab, _boatTransform.position + new Vector3(Random.Range(_minPosition.x, _maxPosition.x),
                                          Random.Range(_minPosition.y, _maxPosition.y), 0), Quaternion.identity);
            _currentTime = 0;
        }
        else
        {
            _currentTime += Time.deltaTime;
        }
    }
}